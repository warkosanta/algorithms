﻿using CryptoLib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlayfairСipher
{
    public class PlayfairСipher : ICipher
    {
        private string key;
        private string alfabet;

        private char[,] cipherMatrix = new char[5,5];


        public PlayfairСipher(string key, string alfabet)
        {
            this.alfabet = alfabet;
            this.key = key;
        }

        public string Encode(string naturalWord)
        {
            FillMatrix();
            var bigramms = BuildBigramms(naturalWord);

            string str = string.Empty;

            foreach (var pair in bigramms)
            {
                int index11 = 0;
                int index12 = 0;

                int index21 = 0;
                int index22 = 0;

                int w = cipherMatrix.GetLength(0); // width
                int h = cipherMatrix.GetLength(1); // height

                for (int x = 0; x < w; ++x)
                {
                    for (int y = 0; y < h; ++y)
                    {
                        if (cipherMatrix[x, y].Equals(pair.Item1))
                        {
                            index11 = x;
                            index12 = y;
                        }

                        if (cipherMatrix[x, y].Equals(pair.Item2))
                        {
                            index21 = x;
                            index22 = y;
                        }
                    }
                }

                // если символы в одной строке
                if (index11 == index21)
                {
                    index12++;
                    index22++;

                    if (index12 == w)
                    {
                        index12 = 0;
                    }
                    if (index22 == w)
                    {
                        index22 = 0;
                    }
                    var temp = index22;
                    index22 = index12;
                    index12 = temp;
                }

                // если символы в одном столбце
                if (index12 == index22)
                {
                    index11++;
                    index21++;

                    if (index11 == h)
                    {
                        index11 = 0;
                    }
                    if (index21 == h)
                    {
                        index21 = 0;
                    }
                }

                str += cipherMatrix[index11, index22];

                str += cipherMatrix[index21, index12];

            }
            return str;
        }

        public string Decode(string cipherWord)
        {
            throw new NotImplementedException();
        }

        private void FillMatrix()
        {
            var newalfabet = alfabet;

            foreach (var c in key)
            {
                newalfabet = newalfabet.Replace($"{c}", "");
            }


            // по времени очередь и связный список работают одинаково

            Queue<char> queue = new Queue<char>(string.Concat(key.Distinct()) + newalfabet);
            LinkedList<char> linkedList = new LinkedList<char>(string.Concat(key.Distinct()) + newalfabet);

            int index1 = 0;
            int index2 = 0;

            while (linkedList.Any())
            {
                cipherMatrix.SetValue(linkedList.First.Value, index1, index2);
                linkedList.RemoveFirst();
                index2++;
                if (index2 != 5) continue;
                index1++;
                index2 = 0;
            }
        }
        private List<Tuple<char, char>> BuildBigramms(string naturalWord)
        {
            var bigramms = new List<Tuple<char, char>>();

            for (int i = 0; i < naturalWord.Length; i += 2)
            {
                if (i + 1 == naturalWord.Length)
                {
                    bigramms.Add(Tuple.Create(naturalWord[i], 'x'));
                    continue;
                }

                if (naturalWord[i] == naturalWord[i + 1])
                {
                    bigramms.Add(Tuple.Create(naturalWord[i], 'x'));
                    i--;
                    continue;
                }

                bigramms.Add(Tuple.Create(naturalWord[i], naturalWord[i + 1]));
            }

            return bigramms;
        }

    }
}
