﻿namespace CryptoLib
{
    /// <summary>
    /// Шифр, позволяющий с помощью ключевого слова заширфровать и расшифровать сообщение.
    /// </summary>
    public interface ICipher
    {

        /// <summary>
        /// Закодировать сообщение на натуральном языке.
        /// </summary>
        /// <param name="naturalWord">Слово или группа слов на естественном яыке.</param>
        /// <returns></returns>
        string Encode(string naturalWord);

        /// <summary>
        /// Расшифровать закодированное сообщение.
        /// </summary>
        /// <param name="cipherWord">Закодированное слово или группа слов.</param>
        /// <returns></returns>
        string Decode(string cipherWord);
    }
}
