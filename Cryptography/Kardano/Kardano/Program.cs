﻿using System;
using System.Linq;

namespace Kardano
{
    class Program
    {
        static int n = 0; // Размерность матрицы

        static void Main(string[] args)
        {
            Console.Write("Введите сообщение: ");
            string message = Console.ReadLine().Replace(" ", string.Empty);

            n = (int)Math.Ceiling(Math.Sqrt(message.Length));

            int[,] mas = CreateRundomMatrix(); // Матрица - трафарет
            string[,] mas2 = new string[n, n]; // Матрица - шифрование

            int counter = 0;
            // Проверка, есть ли в сгенерированной матрице наложения
            bool flag = true;
            while (flag)
            {
                for (int i = 0; i < 4; i++)
                {
                    var newMatrix = Rotate(mas);
                    if (!CompareMatrix(mas, newMatrix))
                    {
                        mas = CreateRundomMatrix();
                        counter++;
                        break;
                    }
                    else
                    {
                        newMatrix = Rotate(newMatrix);
                    }
                    if (i == 3)
                    {
                        flag = false;
                    }
                }
            }

            // Вывод информации
            Console.WriteLine($"Найдена матрица без наложений.\nВсего перебрано матриц: {counter}.");

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    Console.Write("{0}\t", mas[i, j]);
                }
                Console.WriteLine();
            }

            mas2 = Encode(mas, message);

            Console.WriteLine();
            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            Random randomRnd = new Random();
            string decodesStr = "";
            Console.WriteLine("Матрица кодирования:");
            for (int i = 0; i < mas2.GetLength(0); i++)
            {
                for (int j = 0; j < mas2.GetLength(1); j++)
                {

                    decodesStr += mas2[i, j] is null ? alfabet[randomRnd.Next(0, alfabet.Length)].ToString() : mas2[i, j];
                    Console.Write("{0}\t", mas2[i, j] is null ? "-": mas2[i, j]);
                }
                Console.WriteLine();
            }
            Console.Write("\nЗакодированное слово: ");
            Console.WriteLine(decodesStr);
            Console.Write("Было закодировано: ");
            Console.WriteLine(Decode(mas, mas2));
            Console.ReadLine();

        }

        public static string Decode(int[,] keyMatrix, string[,] encodeMatrix)
        {
            string decodeString = string.Empty;
            int counter = 10;
            while (counter != 0)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (keyMatrix[i, j] == 1 && encodeMatrix[i, j] != null)
                        {
                            decodeString += encodeMatrix[i, j].First();
                            if(encodeMatrix[i, j].Length > 1)
                            {
                                string temp = encodeMatrix[i, j];
                                temp = temp.Substring(1, temp.Length - 1);
                                encodeMatrix[i, j] = temp;
                            }
                            else
                            {
                                encodeMatrix[i, j] = null;
                            }
                        }
                    }
                }
                keyMatrix = Rotate(keyMatrix);
                counter--;
            }
            return decodeString;
        }

        public static string[,] Encode(int[,] keyMatrix, string message)
        {
            string[,] encodeMatrix = new string[n, n];
            int counter = 0;
            bool flag = true;
            while (flag)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (keyMatrix[i, j] == 1 && flag)
                        {
                            encodeMatrix[i, j] += message[counter].ToString();
                            counter++;
                            if (counter == message.Length)
                            {
                                flag = false;
                            }
                        }
                    }
                }
                keyMatrix = Rotate(keyMatrix);
            }
            return encodeMatrix;
        }

        public static int[,] Rotate(int[,] m)
        {
            var result = new int[m.GetLength(1), m.GetLength(0)];

            for (int i = 0; i < m.GetLength(1); i++)
                for (int j = 0; j < m.GetLength(0); j++)
                    result[i, j] = m[m.GetLength(0) - j - 1, i];

            return result;
        }   
        public static bool CompareMatrix(int[,] a, int[,] b)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (a[i, j] == b[i, j] && a[i, j] == 1)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public static int[,] CreateRundomMatrix()
        {
            int[,] mas = new int[n, n]; // Матрица - трафарет

            Random rand = new Random();

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = rand.Next(2);
                }
            }

            return mas;
        }
    }
}