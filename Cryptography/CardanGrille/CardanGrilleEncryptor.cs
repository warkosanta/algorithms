﻿using CryptoLib;
using System;

namespace CardanGrille
{
    public class CardanGrilleEncryptor : ICipher
    {
        public string Alfabet { get; }
        private int n;
        public CardanGrilleEncryptor(string alfabet) { Alfabet = alfabet; }

        public string Decode(string decodeWord, string key) => throw new NotImplementedException();
        public string Encode(string encodeWord, string key)
        {
            n = (int)Math.Ceiling(Math.Sqrt(encodeWord.Length));

            int[,] mas = CreateRundomMatrix(); // Матрица - трафарет
            string[,] mas2 = new string[n, n]; // Матрица - шифрование

            int counter = 0;
            // Проверка, есть ли в сгенерированной матрице наложения
            bool flag = true;
            while (flag)
            {
                for (int i = 0; i < 4; i++)
                {
                    var newMatrix = Rotate(mas);
                    if (!CompareMatrix(mas, newMatrix))
                    {
                        mas = CreateRundomMatrix();
                        counter++;
                        break;
                    }
                    else
                    {
                        newMatrix = Rotate(newMatrix);
                    }
                    if (i == 3)
                    {
                        flag = false;
                    }
                }
            }


            mas2 = GetEncodeMatrix(mas, encodeWord);

            Random randomRnd = new Random();
            string decodesStr = "";
            for (int i = 0; i < mas2.GetLength(0); i++)
            {
                for (int j = 0; j < mas2.GetLength(1); j++)
                {
                    decodesStr += mas2[i, j] is null ? Alfabet[randomRnd.Next(0, Alfabet.Length)].ToString() : mas2[i, j];
                }
            }
            return decodesStr;
        }
        private string[,] GetEncodeMatrix(int[,] keyMatrix, string message)
        {
            string[,] encodeMatrix = new string[n, n];
            int counter = 0;
            bool flag = true;
            while (flag)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (keyMatrix[i, j] == 1 && flag)
                        {
                            encodeMatrix[i, j] += message[counter].ToString();
                            counter++;
                            if (counter == message.Length)
                            {
                                flag = false;
                            }
                        }
                    }
                }
                keyMatrix = Rotate(keyMatrix);
            }
            return encodeMatrix;
        }
        private int[,] Rotate(int[,] m)
        {
            var result = new int[m.GetLength(1), m.GetLength(0)];

            for (int i = 0; i < m.GetLength(1); i++)
                for (int j = 0; j < m.GetLength(0); j++)
                    result[i, j] = m[m.GetLength(0) - j - 1, i];

            return result;
        }
        private bool CompareMatrix(int[,] a, int[,] b)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (a[i, j] == b[i, j] && a[i, j] == 1)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private int[,] CreateRundomMatrix()
        {
            int[,] mas = new int[n, n]; // Матрица - трафарет

            Random rand = new Random();

            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = rand.Next(2);
                }
            }

            return mas;
        }
    }
}