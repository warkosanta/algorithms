﻿namespace CryptoLib
{
    /// <summary>
    /// Шифр, позволяющий с помощью ключевого слова заширфровать и расшифровать сообщение.
    /// </summary>
    public interface ICipher
    {
        string Alfabet { get; }

        /// <summary>
        /// Закодировать сообщение на натуральном языке.
        /// </summary>
        /// <param name="encodeWord">Слово или группа слов на естественном яыке.</param>
        /// <returns></returns>
        string Encode(string encodeWord, string key);

        /// <summary>
        /// Расшифровать закодированное сообщение.
        /// </summary>
        /// <param name="decodeWord">Закодированное слово или группа слов.</param>
        /// <returns></returns>
        string Decode(string decodeWord, string key);
    }
}
