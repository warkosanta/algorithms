﻿using CryptoLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PortCipher
{
    public class PortEncoder : ICipher
    {
        public string Alfabet { get; }
        public PortEncoder(string alfabet)
        {
            Alfabet = alfabet;
        }
        public string Decode(string decodeWord, string key)
        {
            throw new NotImplementedException();
        }

        public string Encode(string encodeWord, string key)
        {
            var bigramms = BuildBigramms(Alfabet);
            var encode = GetCode(bigramms, key, encodeWord);      
            return String.Concat(encode);
        }

        private string[] BuildBigramms(string alfabet)
        {
            string[] bigramms = new string[alfabet.Length * alfabet.Length + 1];
            int i = 1;
            foreach (char a in alfabet)
            {
                foreach (char b in alfabet)
                {
                    bigramms[i] = $"{a}{b}";
                    i++;
                }
            }
            return bigramms;
        }

        private IEnumerable<int> GetCode(string[] bigramms, string code, string message)
        {
            List<int> encode = new List<int>();

            for(int i = 0; i < message.Length; i+=code.Length)
            {
                for (int j = 0; j < code.Length; j++)
                {
                    var bigramm = $"{message[i + j]}{code[j]}";
                    for (int k = 0; k < bigramms.Length; k++)
                    {
                        if (String.Compare(bigramms[k], bigramm) == 1)
                        {
                            encode.Add(k);
                            break;
                        }
                    }
                }
            }
            return encode;
        }
    }
}