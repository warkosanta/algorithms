﻿using System.Linq;
using System;
using System.Collections.Generic;
using CryptoLib;

namespace HillCipher
{
    public class HillCipherEncryptor : ICipher
    {
        public string Alfabet { get; }

        private HillCipherEncryptor() { }
        public HillCipherEncryptor(string alfabet)
        {
            Alfabet = alfabet;
        }
        public string Encode(string encodeWord, string key)
        {
            var cipherCode = GetCode(key);
            var cipherMatrix = BuildMatrix(cipherCode);

            var wordCode = GetCode(encodeWord);
            var wordMatrix = BuildMatrix(wordCode, cipherMatrix.GetLength(1));

            List<int> mass = new List<int>();
            foreach (var r in MatrixMultiplication(wordMatrix, cipherMatrix))
            {
                mass.Add(r % Alfabet.Count());
            }

            return String.Concat(GetCode(mass));
        }
        public string Decode(string decodeWord, string key)
        {
            throw new NotImplementedException();
        }

        private int[,] MatrixMultiplication(int[,] matrixA, int[,] matrixB)
        {
            if (matrixA.ColumnsCount() != matrixB.RowsCount())
            {
                throw new ArgumentOutOfRangeException();
            }

            var matrixC = new int[matrixA.RowsCount(), matrixB.ColumnsCount()];

            for (var i = 0; i < matrixA.RowsCount(); i++)
            {
                for (var j = 0; j < matrixB.ColumnsCount(); j++)
                {
                    matrixC[i, j] = 0;

                    for (var k = 0; k < matrixA.ColumnsCount(); k++)
                    {
                        matrixC[i, j] += matrixA[i, k] * matrixB[k, j];
                    }
                }
            }

            return matrixC;
        }

        private int[,] BuildMatrix(IEnumerable<int> wordCode, int rank = 0)
        {
            var stackWord = new Queue<int>(wordCode);

            int n = (int)Math.Floor(Math.Sqrt(wordCode.Count()));
            int[,] matrix = rank == 0? new int[n,n] : new int[n, rank];
            rank = rank == 0 ? n : rank;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < rank; j++)
                {
                    if (stackWord.Count == 0)
                        matrix[i, j] = 35;
                    else
                        matrix[i, j] = stackWord.Dequeue();
                }

            }
            return matrix;
        }

        private IEnumerable<int> GetCode(string word)
        {
            foreach (var ch in word)
            {
                yield return Array.IndexOf(Alfabet.ToCharArray(), ch);
            }
        }
        private IEnumerable<char> GetCode(IEnumerable<int> indexes)
        {
            foreach (var ch in indexes)
            {
                yield return Alfabet[ch]; 
            }
        }
    }
}