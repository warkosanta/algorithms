﻿using CryptoLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlbertiCipher
{
    public class AlbertiEncriptor : ICipher
    {
        public string Alfabet { get; } = "ABCDEFGILMNOPQRSTVXZ1234";
        public AlbertiEncriptor() { }
        public AlbertiEncriptor(string alfabet)
        {
            Alfabet = alfabet;
        }

        public string Decode(string decodeWord, string key)
        {
            throw new NotImplementedException();
        }

        public string Encode(string encodeWord, string key)
        {
            if (key.Length != Alfabet.Length) throw new ArgumentOutOfRangeException();

            var d = BuildDisk(key);
            string encode = string.Empty;
            encodeWord = encodeWord.ToUpper();
            foreach (var c in encodeWord)
            {
                encode += c == ' ' ? ' ' : d[c];
            }

            return encode;
        }

        private Dictionary<char, char> BuildDisk(string key, int step = 0)
        {
            var d = new Dictionary<char, char>();

            for (int i = 0; i < key.Length; i++)
            {
                d.Add(Alfabet[i], key[i]);
            }
            return d;
        }
    }
}
