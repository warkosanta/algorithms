﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace PolybiusSquare
{
    public enum Alphabet { RU = 33 }
    class PolybiusSquare
    {
        private int n; // мощность алфавита
        private char[,] matrix; // матрица алфавита
        private int square;
        private string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        public PolybiusSquare(Alphabet alphabetType)
        {
            this.n = (int)alphabetType;
            square = Convert.ToInt32(Math.Ceiling(Math.Sqrt(n)));
            matrix = new char[square, square];
            SetMatrix(alphabetType);
        }

        public List<int> Encode(string word)
        {
            List<int> decoder = new List<int>(word.Length * 2);

            int k = 0;

            for (int w = 0; w < word.Length; w++)
            {
                for (int i = 0; i < square; i++)
                {
                    for (int j = 0; j < square; j++)
                    {
                        if (matrix[i, j] == word[w])
                        {
                            decoder.Add(i+1);
                            decoder.Add(j+1);
                        }
                    }
                }
            }

            return decoder;
        }

        private void SetMatrix(Alphabet alphabetType)
        {
            if (alphabetType is Alphabet.RU)
            {
                int k = 0;

                for (int i = 0; i < square; i++)
                {
                    for (int j = 0; j < square; j++)
                    {
                        if (k != n)
                        {
                            matrix[i, j] = alphabet[k];
                            k++;
                        }
                    }
                }
            }

            else
            {
                throw new NotImplementedException();
            }
        }
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            var p = new PolybiusSquare(Alphabet.RU);
            var encode = p.Encode("кто");

            for(int i = 0; i < encode.Count; i+=2)
            {
                var row = encode[i];
                while (row != 0)
                {
                    Console.Beep();
                    row--;
                    Thread.Sleep(1000);
                }

                var column = encode[i+1];
                while (column != 0)
                {
                    Console.Beep();
                    column--;
                    Thread.Sleep(500);
                }
            }
           
        }
    }
}
