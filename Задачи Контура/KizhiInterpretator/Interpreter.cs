using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace KizhiPart1
{
    public class Interpreter
    {
        private TextWriter _writer;

        // ���������� �������� � �������.
        private static Dictionary<string, int> vars = new Dictionary<string, int>();

        // ������ �� ��������� �������������� �������.
        private List<Operation> _funcs = new List<Operation>();
        public Interpreter(TextWriter writer)
        {
            _writer = writer;

            // ��������� ������������� ���������.
            _funcs.Add(new Operation(@"set\s\D\s\d", new Action<string, int>((string key, int value) => vars.Add(key, value))));
            _funcs.Add(new Operation(@"sub\s\D\s\d", new Func<string, int, bool>((string key, int value) =>
            {
                int foundValue = 0;
                var sucsess = vars.TryGetValue(key, out foundValue);
                if (sucsess)
                {
                    vars[key] = foundValue - value;
                    return true;
                }
                else
                {
                    return false;
                }
            })));
            _funcs.Add(new Operation(@"print\s\D", new Func<string, object>((string key) => {
                int value;
                var sucsess = vars.TryGetValue(key, out value);
                if (sucsess)
                {
                    return value;
                }
                else
                {
                    return false;
                }
            })));
            _funcs.Add(new Operation(@"rem\s\D", new Func<string, bool>((string key) =>
            {
                int value;
                var sucsess = vars.TryGetValue(key, out value);
                if (sucsess)
                {
                    vars.Remove(key);
                    return true;
                }
                else
                {
                    return false;
                }
            })));
        }

        public void ExecuteLine(string command)
        {
            var operations = _funcs.Find((f) => Regex.IsMatch(command, f.RegexExpression));

            if (operations is null) _writer.WriteLine("������� ���� ������� �������.");

            else
            {
                var result = operations.Deside(command);
                if (result is false) _writer.WriteLine("���������� ����������� � ������");
                else if (result is int) _writer.WriteLine(result);
            }

        }
    }

    /// <summary>
    /// ��������� ����������� ������� ��������������. �������� ��������� ������ � ���� regex ��������� � ��������, 
    /// � ����� ��������������� �����, ������������ ��������������� �� ����t���� ������� ���������.
    /// </summary>
    public interface IOperation
    {
        string RegexExpression { get; set; }
        Delegate Delegate { get; set; }
        object Deside(string command);
    }
    /// <summary>
    /// ������� ��������������.
    /// </summary>
    public class Operation : IOperation
    {
        /// <summary>
        /// ������-�������, ����������� ��������� ������.
        /// </summary>
        public string RegexExpression { get; set; }

        /// <summary>
        /// ����������� �����.
        /// </summary>
        public Delegate Delegate { get; set; }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="regex">�������-��������� ������.</param>
        /// <param name="predicate">�����.</param>
        public Operation(string regex, Delegate predicate)
        {
            RegexExpression = regex;
            Delegate = predicate;
        }

        /// <summary>
        /// ����������, ������������� �� �������� ������� ��������� ������.
        /// </summary>
        /// <param name="command">�������� �������.</param>
        /// <returns>���������� ������, ���� �������� �������, � null ���� ���.</returns>
        public object Deside(string command)
        {
            int commElements = 3; // ������������ ���������� ��������� � �������.
            object obj = null;
            if (Regex.IsMatch(command, RegexExpression))
            {
                var args = command.Split(' ');
                if (args.Length == commElements)
                {
                    obj = Delegate.DynamicInvoke(args[1], int.Parse(args[2]));
                }
                else
                {
                    obj = Delegate.DynamicInvoke(args[1]);
                }
            }
            return obj;
        }
    }
}