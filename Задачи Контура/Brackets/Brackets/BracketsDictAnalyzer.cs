﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Brackets
{
    public partial class BracketsAnalyzer
    {
        /// <summary>
        /// Вариант с соблюдением принципа DRY. Просто расширять, изменять.
        /// </summary>
        /// <remarks>
        /// Судя по всему ContainsValue/Key может обеспечить как сложность O(1) так и O(n), foreach также имеет сложность O(n).
        /// Время прохождения тестов 7 мс => сложность всей реализации не О(n),а O(n^2).
        /// При том, если использовать Keys/Values.Contains метод из System.Linq время прохождения тестов возрастает до 8-9 мс.
        /// Следовательно, использование методов ContainsValue/Key действительно предпочтительнее,
        /// поскольку уменьшает время вычислений и не требует подключения дополнительного неймспейса.
        /// </remarks>
        public bool DictAnalyze(string bracketsStrng)
        {
            Stack<char> stack = new Stack<char>();
            Dictionary<char, char> bracketsPairs = new Dictionary<char, char>();

            bracketsPairs['['] = ']';
            bracketsPairs['('] = ')';
            bracketsPairs['{'] = '}';
            bracketsPairs['<'] = '>';

            foreach (var symbol in bracketsStrng)
            {
                if (bracketsPairs.Keys.Contains(symbol)) stack.Push(symbol);
                else if (bracketsPairs.Values.Contains(symbol))
                {
                    if (stack.Count == 0) return false;
                    var bracket = stack.Pop();
                    if (bracketsPairs[bracket] != symbol) return false;
                }
                else
                {
                    return false;
                }
            }
            return stack.Count == 0;
        }

        /// <summary>
        /// Вариант с соблюдением принципа DRY. Просто расширять, изменять.
        /// Span заместо Stack. 
        /// </summary>
        /// <param name="bracketsStrng"></param>
        /// <returns></returns>
        public bool DictSpanAnalyze(string bracketsStrng)
        {
            if (bracketsStrng.Length % 2 == 1) return false;
            Span<char> spanStack = new Span<char>(new char[bracketsStrng.Length / 2]);
            Dictionary<char, char> bracketsPairs = new Dictionary<char, char>();

            bracketsPairs['['] = ']';
            bracketsPairs['('] = ')';
            bracketsPairs['{'] = '}';
            bracketsPairs['<'] = '>';
            int counter = 0;
            foreach (var symbol in bracketsStrng)
            {
                if (bracketsPairs.Keys.Contains(symbol))
                {
                    spanStack[counter] = symbol;
                    counter++;
                }
                else if (bracketsPairs.Values.Contains(symbol))
                {
                    if (spanStack.Length == 0) return false;
                    var bracket = spanStack[counter - 1];
                    counter--;
                    if (bracketsPairs[bracket] != symbol) return false;
                }
                else
                {
                    return false;
                }
            }
            return counter == 0;
        }
    }
}