﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Brackets
{
    /// <summary>
    /// Решение задачи правильной скобочной последовательности.
    /// </summary>
    public partial class BracketsAnalyzer
    {
        /// <summary>
        /// Вариант без соблюдением принципа DRY. Сложно расширять, изменять.
        /// </summary>
        /// <remarks>
        /// Сложность O(n).
        /// Время прохождения тестов 4 мс.
        /// </remarks>
        public bool CaseAnalyze(string bracketsStrng)
        {
            Stack<char> stack = new Stack<char>();
            foreach(var symbol in bracketsStrng)
                switch (symbol)
                {
                    case '(':
                    case '[':
                    case '{':
                    case '<':
                        {
                            stack.Push(symbol);
                            break;
                        }
                    case ')':
                        {
                            if (stack.Count == 0) return false;
                            var lastBracket = stack.Pop();
                            if (lastBracket != '(') return false;
                            break;
                        }
                    case ']':
                        {
                            if (stack.Count == 0) return false;
                            var lastBracket = stack.Pop();
                            if (lastBracket != '[') return false;
                            break;
                        }
                    case '}':
                        {
                            if (stack.Count == 0) return false;
                            var lastBracket = stack.Pop();
                            if (lastBracket != '{') return false;
                            break;
                        }
                    case '>':
                        {
                            if (stack.Count == 0) return false;
                            var lastBracket = stack.Pop();
                            if (lastBracket != '<') return false;
                            break;
                        }
                    default: return false;
                }
            return stack.Count == 0;
        }

        /// <summary>
        /// Вариант без соблюдением принципа DRY. Сложно расширять, изменять.
        /// Span заместо Stack.
        /// </summary>
        /// <param name="bracketsStrng"></param>
        /// <returns></returns>
        public bool RefactoredSpanCaseAnalyze(string bracketsStrng)
        {
            if (bracketsStrng.Length % 2 == 1) return false;
            Span<char> spanStack = new Span<char>(new char[bracketsStrng.Length / 2]);
            int counter = 0;

            foreach (var symbol in bracketsStrng)
            {
                switch (symbol)
                {
                    case '(':
                    case '[':
                    case '{':
                    case '<':
                        {
                            spanStack[counter] = symbol;
                            counter++;
                            break;
                        }
                    case ')':
                    case '}':
                    case ']':
                    case '>':
                        {
                            if (spanStack.Length == 0) return false;
                            var bracket = spanStack[counter - 1];
                            counter--;
                            if (ReverseBrackets(bracket) != symbol) return false;
                            break;
                        }
                    default: return false;
                }
            }
            return counter == 0;
        }

        /// <summary>
        /// Вариант с соблюдением принципа DRY. По преждему сложно расширять, изменять.
        /// </summary>
        /// <remarks>
        /// Сложность O(n).
        /// Время прохождения тестов 4 мс.
        /// </remarks>
        public bool RefactoredCaseAnalyze(string bracketsStrng)
        {
            var stack = new Stack<char>();
            foreach(var symbol in bracketsStrng)
            {
                switch (symbol)
                {
                    case '(':
                    case '[':
                    case '{':
                    case '<':
                        {
                            stack.Push(symbol);
                            break;
                        }
                    case ')':
                    case '}':
                    case ']':
                    case '>':
                        {
                            if (stack.Count == 0) return false;
                            var bracket = stack.Pop();
                            if(ReverseBrackets(bracket) != symbol) return false;
                            break;
                        }
                    default: return false;
                }
            }
            return stack.Count == 0;
        }

        private char ReverseBrackets(char bracket)
        {
            switch (bracket)
            {
                case '(':
                    return ')';
                case '[':
                    return ']';
                case '{':
                    return '}';
                case '<':
                    return '>';
                default: throw new ArgumentException(message: "Argument was an incorrect symbol: " + bracket);
            }
        }
    }
}