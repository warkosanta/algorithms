﻿using BenchmarkDotNet.Attributes;
using Brackets;

namespace BracketsTests
{
    [MemoryDiagnoser]
    [Orderer(BenchmarkDotNet.Order.SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class BracketsBenchmarks
    {
        private const string brakets = "(({})[])";
        private static readonly BracketsAnalyzer bracketsAnalyzer = new BracketsAnalyzer();

        [Benchmark]
        public void CaseAnalyze()
        {
            bracketsAnalyzer.CaseAnalyze(brakets);
        }

        [Benchmark]
        public void DictAnalyze()
        {
            bracketsAnalyzer.DictAnalyze(brakets);
        }

        [Benchmark]
        public void DictSpanAnalyze()
        {
            bracketsAnalyzer.DictSpanAnalyze(brakets);
        }

        [Benchmark]
        public void RefactoredCaseAnalyze()
        {
            bracketsAnalyzer.RefactoredCaseAnalyze(brakets);
        }

        [Benchmark]
        public void RefactoredSpanCaseAnalyze()
        {
            bracketsAnalyzer.RefactoredSpanCaseAnalyze(brakets);
        }
    }
}
