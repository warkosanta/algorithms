﻿using BenchmarkDotNet.Running;
using Brackets;
using BracketsTests;

namespace BenchmarkDotNetTests
{
    class Program
    {
        static void Main(string[] args)
        {
           BenchmarkRunner.Run<BracketsBenchmarks>();
        }
    }
}
