﻿using PortCipher;
using Xunit;

namespace AllPurposeTests
{
    public class PortCipherTests
    {
        public class PortCipherEncodeTests
        {
            private static protected PortEncoder cipher = new PortEncoder("абвгдежзиклмнопрстуфхцчшщъыьэюя");

            [Theory]
            [InlineData("aaв", "a", "1163")]
            [InlineData("на марсе будут цвести яблони", "луна", "38420134312485510157151572126570547165374175510529260194433322423386250")]
            public void TestPort(string encodeWord, string key, string result)
            {
                var actual = cipher.Encode(encodeWord.Trim(), key);
                Assert.Equal(result, actual);
            }
        }
    }
}
