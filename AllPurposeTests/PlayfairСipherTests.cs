using CryptoLib;
using Xunit;

namespace XUnitTestProject1
{
    public class Playfair�ipherTests
    {
        [Theory]
        [InlineData("home", "OWNS")]
        [InlineData("television", "SCGTXGRFPO")]
        [InlineData("freedom", "ISRVTYPN")]
        public void AllNumbers_AreOdd_WithInlineData(string naturalWord, string cipherWord)
        {
            // Arrange
            Playfair�ipher.Playfair�ipher cipherEN = new Playfair�ipher.Playfair�ipher(key: "secretkey",
                alfabet: "abcdefghiklmnopqrstuvwxyz");

            // Act
            var result = cipherEN.Encode(naturalWord).ToUpper();

            // Assert
            Assert.Equal(cipherWord, result);

        }
    }
}