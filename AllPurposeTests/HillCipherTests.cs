﻿using HillCipher;
using Xunit;

namespace СipherTests
{
    /// <summary>
    /// Тесты для шифра Хилла.
    /// </summary>
    public class HillCipherTests
    {
        [Theory]
        [InlineData("шифр", "альпинизм", "АЮНЧХЯ")]
        public void HillCipherEncryptor_Encode(string naturalWord, string key, string encode)
        {
            // Arrange
            HillCipherEncryptor cipher = new HillCipherEncryptor(alfabet: "абвгдеёжзийклмнопрстуфхцчшщъыьэюя., ?");

            // Act
            var result = cipher.Encode(naturalWord, key);

            // Assert
            Assert.Equal(encode.ToLower(), result);

        }
    }
}
