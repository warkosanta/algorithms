﻿using AlbertiCipher;
using Xunit;

namespace AllPurposeTests
{
    /// <summary>
    /// Тесты для шифра Альберти.
    /// </summary>
    public class AlbertiCipherTests
    {
        private static protected AlbertiEncriptor cipher = new AlbertiEncriptor();

        [Theory]
        [InlineData("gklnprtuz&xysomqihfdbace", "cat", "lgi")]
        [InlineData("gklnprtuz&xysomqihfdbace", "cat dog", "lgi nyt")]
        [InlineData("egklnprtuz&xysomqihfdbac", "zoo animal", "fxx e&tzeu")]
        [InlineData("cegklnprtuz&xysomqihfdba", "love coding", "t&ql g&krzp")]
        public void TestAlberti(string key, string encodeWord, string result)
        {
            var actual = cipher.Encode(encodeWord, key);
            Assert.Equal(result, actual);
        }
    }
}