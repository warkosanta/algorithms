using Brackets;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace BracketsTests
{ 
    /// <summary>
    /// ����� ��������� ������� ������ ��
    /// ����������� ������������ ��������� ������������������.
    /// </summary>
    public class BracketsTests
    {
        private static readonly BracketsAnalyzer bracketsAnalyzer = new BracketsAnalyzer();

        // �������� ������ � ���������� ClassData.
        // �� ����� ������������, �� ������ ����.

        [Theory]
        [ClassData(typeof(BracketsTestData))]
        public void CaseAnalyze(string brackets, bool expected)
        {
            var result = bracketsAnalyzer.CaseAnalyze(brackets);

            Assert.Equal(expected, result);
        }

        [Theory]
        [ClassData(typeof(BracketsTestData))]
        public void RefactoredCaseAnalyze(string brackets, bool expected)
        {
            var result = bracketsAnalyzer.RefactoredCaseAnalyze(brackets);

            Assert.Equal(expected, result);
        }

        [Theory]
        [ClassData(typeof(BracketsTestData))]
        public void DictAnalyze(string brackets, bool expected)
        {
            var result = bracketsAnalyzer.DictAnalyze(brackets);

            Assert.Equal(expected, result);
        }

        // � ��������� ���� ��������� ��� �������� ��� ������ Span,
        // ��� �������� ������������� ������� ������

        [Theory]
        [ClassData(typeof(BracketsTestData))]
        public void DictSpanAnalyze(string brackets, bool expected)
        {
            var result = bracketsAnalyzer.DictSpanAnalyze(brackets);

            Assert.Equal(expected, result);
        }

        // ������� ������������ � ���������� ������ ������ ������.
        // ������������, �� ����� ����.
        // RefactoredSpanCaseAnalyze �������� �������� ������� �� ���� ��������� �������.

        private void Test(string bracketsString, bool result)
        {
            Assert.Equal(bracketsAnalyzer.RefactoredSpanCaseAnalyze(bracketsString), result);
        }

        [Fact]
        public void EmptyString()
        {
            Test("", true); 
        }

        [Fact]
        public void ManyBrackets()
        {
            Test("([{<>}])", true);
        }


        [Fact]
        public void Complacated1()
        {
            Test("([<()]>)", false);
        }

        [Fact]
        public void Complacated2()
        {
            Test("([<()>])", true);
        }

        [Fact]
        public void NoBrackets()
        {
            Test("no brackets", false);
        }

        [Fact]
        public void NoClosingBracket()
        {
            Test("([]", false);
        }

        [Fact]
        public void NoOpenningBracket()
        {
            Test("[])", false);
        }

        [Fact]
        public void WrongSymbols()
        {
            Test("[*]", false);
        }
    }

    /// <summary>
    /// �������� ������ ��������� �������������������.
    /// </summary>
    public class BracketsTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]{ "", true };
            yield return new object[] { "([{<>}])", true };
            yield return new object[] { "([<()]>)", false };
            yield return new object[] { "([<()>])", true };
            yield return new object[] { "no brackets", false };
            yield return new object[] { "([]", false };
            yield return new object[] { "[])", false };
            yield return new object[] { "[*]", false };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}