﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            n = 7;
            m = 4;
            //used = new bool[] {false, false, false, false, false,false,false,false};
            a = new int[n*2];
            ////m = new[] {'a', 'b'};
            //ReSlog(0,0,1);
            Rec(0);
            //    //var res = perest.Where(i => !i.Contains("**")).Reverse().ToList();
               var res = perest.Where(str => DictAnalyze(str) is true);
            Console.WriteLine(res.ElementAt(8232));
            //Console.WriteLine(DictAnalyze("(()[([][]())[()][()][][]])([])()"));

        }

        //public static int n;
        //public static int[] mass; 
        //public static void ReSlog(int idx, int sum, int last)
        //{
        //    if (sum == n)
        //    {
        //        Out(idx);
        //        return;
        //    }

        //    for (int i = last; i <= n - sum; i++)
        //    {
        //        mass[idx] = i;
        //        ReSlog(idx+1,sum + i, i);
        //    }
        //}



        public static int n;
        public static int m;
        public static int[] mass;

        // 0 = -
        // 1 = *
        public static void RecFish(int idx)
        {
            if (idx == n)
            {
                Out();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                //if (idx != 0)
                //{
                //    if (mass[idx - 1] == 1)
                //    {
                //        continue;
                //    }
                //}
                mass[idx] = i;
                RecFish(idx + 1);
            }
        }

        public static bool DictAnalyze(string bracketsStrng)
        {
            Stack<char> stack = new Stack<char>();
            Dictionary<char, char> bracketsPairs = new Dictionary<char, char>();

            bracketsPairs['['] = ']';
            bracketsPairs['('] = ')';
            bracketsPairs['{'] = '}';
            bracketsPairs['<'] = '>';

            foreach (var symbol in bracketsStrng)
            {
                if (bracketsPairs.Keys.Contains(symbol)) stack.Push(symbol);
                else if (bracketsPairs.Values.Contains(symbol))
                {
                    if (stack.Count == 0) return false;
                    var bracket = stack.Pop();
                    if (bracketsPairs[bracket] != symbol) return false;
                }
                else
                {
                    return false;
                }
            }
            return stack.Count == 0;
        }



        //public static int n;
        //public static int m;
        //public static int[] mass;

        //// 0 = -
        //// 1 = *
        //public static void RecFish(int idx)
        //{
        //    if (idx == n)
        //    {
        //        Out();
        //        return;
        //    }

        //    for (int i = 0; i < 2; i++)
        //    {
        //        //if (idx != 0)
        //        //{
        //        //    if (mass[idx - 1] == 1)
        //        //    {
        //        //        continue;
        //        //    }
        //        //}
        //        mass[idx] = i;
        //        RecFish(idx + 1);
        //    }
        //}


        /// <summary>
        /// перестоновки без повторений
        /// </summary>
        //public static bool[] used;
        //public static void Rec(int number)
        //{
        //    if (number == n)
        //    {
        //        Out();
        //        return;
        //    }
        //    for (int i = 1; i <= n; i++)
        //    {
        //        if (!used[i])
        //        {
        //            mass[number] = i;
        //            used[i] = true;
        //            Rec(number + 1);
        //            used[i] = false;
        //        }
        //    }

        //}


        //public static int n; // длина 
        //public static int m; // 1,2,3 - символы

        //public static int[] a;
        public static List<string> perest = new List<string>();

        public static int[] a;

        // <summary>
        // рекурсивный перебор
        // </summary>
        // <param name = "dx" ></ param >
        public static void Rec(int dx)
        {
            if (dx == n*2)
            {
                Out();
                return;

            }

            for (int i = 1; i <= m; i++)
            {
                a[dx] = i;
                Rec(dx + 1);
            }
        }

        private static void Out()
        {
            
            perest.Add(string.Concat(a).Replace('1', '(').Replace('2', ')').Replace('3','[').Replace('4',']'));
            
            //perest.Add(string.Concat(a));
            //Console.WriteLine(string.Concat(a));
        }

        // нахождение факториала с помощью рекурсии
        public static int RecFactorial(int n) 
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * RecFactorial(n - 1);
            }
        }

        public static int RecFibonachi(int n)
        {
            if (n == 0)
            {
                return 0;
            }

            if (n == 1)
            { 
                return 1;
            }

            return RecFibonachi(n - 1) + RecFibonachi(n - 2);
        }
    }
}
