﻿using System;

namespace Week1
{
    public class Class1
    {
        // нахождение факториала с помощью рекурсии
        private int n = 3;
        private int counter = 1;
        public void RecFactorial(int number)
        {
            counter++;
            
            var res = number * counter;

            if (counter == n)
            {
                Console.WriteLine(res);
            }
            else
            {

                RecFactorial(res);
            }
        }
    }
}
